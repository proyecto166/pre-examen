function ajax() {
    let idbuscar = document.getElementById('id').value;

    const url = "https://jsonplaceholder.typicode.com/users";
    axios.get(url, {
        params: {
            id: idbuscar
        }
    })
    .then(res => {
        mostrar(res.data);
    })
    .catch((err)=> {
        console.error("kekeo:" + err);
    });

    function mostrar(data) {
        for (let item of data) {
            if (item.id == idbuscar) {
                document.getElementById('nombre').value = item.name;
                document.getElementById('nombreUsuario').value = item.username;
                document.getElementById('email').value = item.email;
                document.getElementById('calle').value = item.address.street;
                document.getElementById('numero').value = item.address.suite;
                document.getElementById('ciudad').value = item.address.city;
            }
        }
    }

}



const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', function(){
    ajax();
});